// Fill out your copyright notice in the Description page of Project Settings.

#include "sgd240_spike_4.h"
#include "MainGameStateBase.h"

// constructor to Initialize the score
AMainGameStateBase::AMainGameStateBase()
{
	CollectibleScore = 0;
}

// Called by MainGameModeBase when the game begins to reset the score.
void AMainGameStateBase::ResetScore()
{
	CollectibleScore = 0;
	UE_LOG(LogTemp, Warning, TEXT("Score was reset"));
}

// Increment the score
void AMainGameStateBase::AddScore()
{
	// Error checking
	if(CollectibleScore == NULL)
	{
		CollectibleScore = 0;
		UE_LOG(LogTemp, Warning, TEXT("Score was null, resetting"));		
	}

	// Increment the score
	CollectibleScore++;

	// Print in the log
	FString ScoreString = FString::FromInt(CollectibleScore);
	UE_LOG(LogTemp, Log, TEXT("Score is: %s"), *ScoreString);

}

int AMainGameStateBase::GetCollectibleScore()
{
	return CollectibleScore;
}
