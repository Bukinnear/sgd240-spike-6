---
title: Spike Report
---

Core 6 – Packaging an executable
================================

Introduction
------------

We’ve now got a nearly complete, testable game.

We need to be able to distribute it to people who are not on our
development team – so it’s time to package the game up into an
Executable!

Our game currently targets PC, but we could use a similar process to
learn how to deploy for Android, iOS, or a Console.

Goals
-----

Building on Core Spikes 4 and 5:

1.  Package your project into an Executable

2.  Document what options you selected, if any, and what effects they
    had on either the Packaging process or the Execution of the game
    (i.e. frame-rate differences, load-time, or packaging-time).

3.  Knowledge: What is “Cooking” in the Unreal Engine?

Personnel
---------

In this section, list the primary author of the spike report, as well as
any personnel who assisted in completing the work.

  ------------------- -----------------
  Primary – Jared K   Secondary – N/A
  ------------------- -----------------

Technologies, Tools, and Resources used
---------------------------------------

1.  [Unreal Engine Documentation on Packaging and
    Cooking](https://docs.unrealengine.com/latest/INT/Engine/Deployment/)

Tasks undertaken
----------------

Packaging:

1.  To package the game: File -&gt; Package -&gt; Windows 32 bit

Options:

1.  ENABLE: Full Rebuild

2.  ENABLE: For Distribution

What we found out
-----------------

How to package a game:

1.  File -&gt; Package -&gt; \[Choose the platform you wish to target\]

Options:

1.  Options for packaging can be changed in Project Settings -&gt;
    Packaging

Full Rebuild:

1.  This option rebuilds the project from scratch, as opposed to simply
    building newly changed files – it is strongly recommended for
    shipping builds to get a clean slate and ensure there are no
    unexpected side effects. The drawback is that this takes a lot
    longer to package.

For Distribution:

1.  This option, as the name implies is configured for a shipping title.

\[Optional\] Open Issues/risks
------------------------------

During packaging, I encountered an Unknown Cooking Error Caused by a BP
file with “an inappropriate outermost.” – The solution to this problem
was to go into the BP of the file, and make any minor edit to force a
recompile, compile, then reverse the edit and compile again.
